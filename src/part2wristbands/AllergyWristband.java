package part2wristbands;

public class AllergyWristband extends Wristband {
    
    
    private String allergy;

    //Constructor
    public AllergyWristband(String barcode, String information, String allergy) {
        super(barcode, information);
        this.allergy = allergy;
    }   
    
    //getter
    public String getAllergicTo(){
        return this.allergy;
    }
    
    //toString
    @Override
    public String toString(){
        return super.toString() + " " + getAllergicTo();
    }

}
