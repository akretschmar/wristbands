package part2wristbands;


public class Wristband {
    private String barcode;
    private String information;
    
   //constructors
    public Wristband(String barcode, String information){
        this.barcode = barcode;
        this.information = information;
    }
   
    //getters
    public String getBarcode(){
        return this.barcode;
    }
    
    public String getInformation(){
        return this.information;
    }
    
    @Override
    public String toString(){
        return this.barcode + " " + this.information;
    }
}
