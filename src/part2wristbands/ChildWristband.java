package part2wristbands;


public class ChildWristband extends Wristband{
    private String parentName;
    
    //constrcutors
public ChildWristband(String barcode, String information, String parentName) {
        super(barcode, information);
        this.parentName = parentName;
    }
    
public String getParentName(){
    return parentName;
}

@Override
public String toString(){
    return super.toString() + " Parent/Guardian: " + getParentName();
}
}
