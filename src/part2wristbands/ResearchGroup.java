
package part2wristbands;


import java.util.List;

public class ResearchGroup {
    private List<Patient> patients;
    private int waitTime;
    
    public ResearchGroup(){}
    public ResearchGroup (List<Patient> patients){
        this.patients = patients;
    }
    
    public List<Patient> getListOfPatients(){
        return patients;
    }
    
    public int getWaitTime(){
        return this.waitTime;
    }

    
}
