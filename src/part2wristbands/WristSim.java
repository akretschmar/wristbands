package part2wristbands;

import java.util.ArrayList;
import java.util.List;


public class WristSim {

    public static void main(String[] args) {
        
        
        
        //Making Allergy Wristbands
        AllergyWristband aw1 = new AllergyWristband("Ae011", "PKD", "Penicillin allergy");
        AllergyWristband aw2 = new AllergyWristband("Ae012", "PKD", "Sulfonomide allergy");
        
        
        //Child Wristbands
        ChildWristband cw1 = new ChildWristband("Ae333", "","Norma Frank");
        ChildWristband cw2 = new ChildWristband("Ae335", "","Gina Florence");
        
        //Creating specific wristbands for patients
        //Patient 1
        List<Wristband> p1wb = new ArrayList<>();
        p1wb.add(cw1);
        p1wb.add(aw1);
        
        Patient p1 = new Patient("Cindy Burns", "02/24/2019", "B. Brown", p1wb);
        
        
        //Patient 2
        List<Wristband> p2wb = new ArrayList<>();
        p2wb.add(cw2);
        p2wb.add(aw2);
        
        Patient p2 = new Patient("Jared Mac", "12/22/2017", "C. Dredon", p2wb);
        
        //Put patients 1 and 2 into a list that will be added to the research group
        List<Patient> patients = new ArrayList<>();
        patients.add(p1);
        patients.add(p2);
        
         //Making ResearchGroup to add list of patients to        
        ResearchGroup researchGroup1 = new ResearchGroup(patients);
       
        List<Patient> ptnt = researchGroup1.getListOfPatients();
        
        //Print patients
        for(Patient ptn : ptnt){
                System.out.println("--Patient--\n" + ptn.getPatientInfo());
        }
        
        
        
    }
    
}
